require 'rubygems'
require 'sinatra'
require 'sinatra/base'
require 'sinatra/param'
require 'sinatra/namespace'
require 'sinatra/reloader'
require "jwt"
require "securerandom"


require File.expand_path(File.join('app/helpers', 'application_helper'))
require File.expand_path(File.join('app/controllers', 'application_controller'))

Dir[File.join('app/helpers', '**/*._helper.rb')].each { |file| require File.expand_path(file) }
Dir[File.join('app/controllers', '**/*_controller.rb')].each { |file| require File.expand_path(file) }
Dir[File.join('app/services', '**/*_service.rb')].each { |file| require File.expand_path(file) }



#Dir[File.join('models', '**/*.rb')].each { |file| require File.expand_path(file) }
