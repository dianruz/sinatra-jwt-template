require "json"

class HomeController < ApplicationController

   get '/' do
      "hello root"
   end

   get '/home' do
      "hello home"
   end


end
