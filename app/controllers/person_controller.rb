require "json"

class PersonController < ApplicationController


   get '/login' do
      erb :"home/login", :layout => :master_page
   end

   get '/logout' do
     session["access_token"] = nil
     redirect to("/login")
   end

   post '/login' do

     if params[:username] == "dale" && params[:password] == "dale"

       headers = {
         alg: ENV['JWT_ALGORITHM'],
         iss: ENV['JWT_ISSUER'],
         typ: 'JWT'
       }

       payload = {
          user_id: params[:username],
          exp: Time.now.to_i + ENV['JWT_EXP_MINUTES'].to_i   #expire in 30 seconds
       }

       @token = JWT.encode(payload, ENV['JWT_SECRET'], ENV['JWT_ALGORITHM'], headers)
       puts "token after login: #{@token}"

       session["access_token"] = @token

       puts "isi session after login: #{session}"


       redirect to("/list")
     else
       @message = "Username/Password failed."
       erb :"home/login", :layout => :master_page
     end
   end

   get "/home" do
      puts "session: #{session['access_token']}"
      "halaman home #{session[:access_token]}"
   end

   get '/list' do
      protected!

      title "Example Page"

      p = PersonService.new

      @nama = p.sayHello("Abi")

      data = []

      for x in 1..1000 do
         data.push({ id: x, nama: 'dale', alamat: 'semarang' })
      end

      @table = data
      @person_list = p.getPerson()

      content_type :json

      { hasil: @person_list }.to_json
      #erb :"home/person", :layout => :master_page
   end

   get '/' do

      redirect to("/person/list")
   end

end
