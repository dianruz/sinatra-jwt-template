require 'rack/protection'
require_relative "../helpers/settings_helper"

class ApplicationController < Sinatra::Base
   helpers ApplicationHelper, Sinatra::Param

   register Sinatra::Namespace

   #secure rack protection
   use Rack::Protection

   enable :sessions
   set :session_secret, ENV['SESSION_SECRET'] { SecureRandom.hex(64) }
   set :session_store, Rack::Session::Pool

   # set public folder for static files
    set :public_folder, File.expand_path('../../public', __FILE__)

    # set folder for templates to ../views, but make the path absolute
    set :views, File.expand_path('../../views', __FILE__)

    # don't enable logging and reloading when running tests
     configure :production, :development do
       register Sinatra::Reloader
       enable :logging
     end

     #will be used to display 404 error pages
     not_found do
       title 'Not Found!'
       erb :not_found
     end

end
