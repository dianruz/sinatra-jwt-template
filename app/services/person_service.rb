require "sequel"

class PersonService
   DB = Sequel.connect(adapter: 'postgres', host: 'localhost', database: 'dbname', user: 'postgres', password: 'xxx')

   def getPerson()
      result = DB.fetch("select * from person").all

      hashes = result.collect { |ro|
        ro.to_hash
      }

      #JSON.generate(hashes)
   end

   def sayHello(nama)
      "nama saya #{nama}"
   end

end
