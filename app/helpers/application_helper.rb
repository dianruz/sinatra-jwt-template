require_relative "settings_helper"

module ApplicationHelper
   def title(value = nil)
      @title = value if value
      @title ? "Controller Demo - #{@title}" : "Controller Demo"
   end

   #-- JWT helper ***************************************************************

   def protected!
      return if authorized?
      redirect('/person/login')
   end

   def extract_token
      token = request.env["HTTP_ACCESS_TOKEN"]
      #token = env.fetch('access_token', '') #.sub(/\A\s*access_token:\s*/i, '')
      puts "token1 : #{token}"

      if token
           return token
      end
      token = request["HTTP_ACCESS_TOKEN"]
      puts "token2: #{token}"

      if token
           return token
      end
      token = session["access_token"]
      puts "token from session: #{session[:access_token]}"

      if token
           return token
      end

      return nil
   end

   def refresh_token(payload, headers)
      payload[:exp] = Time.now.to_i + ENV['JWT_EXP_MINUTES'].to_i   #expire in 30 seconds

      token_refresh = JWT.encode(payload, ENV['JWT_SECRET'], ENV['JWT_ALGORITHM'], headers)
      puts "token refresh: #{token_refresh}"

      session["access_token"] = token_refresh
   end

   def authorized?
      @token = extract_token
      puts "algor: #{ENV['JWT_ALGORITHM']} | #{ENV['JWT_ISSUER']} | #{ENV['JWT_SECRET']}"

      begin
           payload, header = JWT.decode @token, ENV['JWT_SECRET'], true, { algorithm: ENV['JWT_ALGORITHM'], iss: ENV['JWT_ISSUER'] }

           @exp = payload["exp"]
           puts "exp: #{@exp}"

           if @exp.nil?
               puts "Access token doesn't have exp set"
               return false
           end

           @exp = Time.at(@exp.to_i)

           if Time.now > @exp
               puts "Access token expired"
               return false
           end

           #refresh token to session
           refresh_token(payload, headers)

           @user_id = payload["user_id"]

      rescue JWT::DecodeError => ex
           puts "An error of type #{ex.class} happened, message is #{ex.message}"
           return false
      end


   end

end
